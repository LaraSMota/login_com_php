<?php 
    session_start();
    $usuario = $_POST['login'];
    $senha = $_POST['senha'];

    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";

    // $usuarios = array('marcos', 'diego', 'lucas');
    // $senhas = array(123, 234, 345);

    // $autenticado = false;
    
    // foreach($usuarios as $indice => $variavel){
    //     if($usuario == $variavel && $senha == $senhas[$indice]){
    //         $autenticado = true;
    //     }
    // }
    // echo '<br><br>';

    $autenticado = false;

    $usuarios = array(
        array('login' => 'marcos', 'senha' => '111'),
        array('login' => 'lucas', 'senha' => '228'),
        array('login' => 'pedro', 'senha' => '135'),
        array('login' => 'lara', 'senha' => '169'),
    );
    
    foreach($usuarios as $user){
        if($usuario == $user['login'] && $senha == $user['senha']){
            $autenticado = true;
        }
    }

    if($autenticado){
        $_SESSION['usuario'] = $usuario;
        header('Location: home.php');
    } else {
        header('Location: index.php?login=erro');
    }
?>